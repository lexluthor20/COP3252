//Author: 

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;

public class PictureFrame extends JPanel
{
  //paints the image, calls paintCircle
  //and paintRectangle
  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    setBackground(Color.BLUE);
    paintCircle(g);
    paintRectangle(g);
  }

  public void paintCircle(Graphics graph)
  {
    graph.setColor(Color.YELLOW);

    //finding the smaller of the two dimensions
    //and setting the size to 1/4th of it
    if(getWidth() < getHeight())
      size = getWidth()/4;
    else
      size = getHeight()/4;

    x = getWidth()*0.7;
    y = getHeight()*0.1;

    graph.fillOval((int)x,(int)y,size,size);
  }

  public void paintRectangle(Graphics graph)
  {
    graph.setColor(new Color(139,69,19));

    y = getHeight()*0.9;

    graph.fillRect(0,(int)y,getWidth(),getHeight());
  }

  private int size;
  private double x;
  private double y;
}
