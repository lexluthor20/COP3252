//Author: 

import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Color;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MovingPicture extends JPanel
{
  //constructor
  public MovingPicture(JDesktopPane desk)
  {
    //creating a new JInternalFrame
    frame = new JInternalFrame("Movable Picture Frame",
                                  true,true,true,true);
    //adding the InternalFrame
    //to the DesktopPane
    desk.add(frame);
         
    //creating a box layout, putting the checkbox container
    //at the top and the moving picture in the center
    setLayout(new BorderLayout());
    cbox = new JPanel();
    frame.add(cbox, BorderLayout.NORTH);
    frame.add(this, BorderLayout.CENTER);

    //creating a checbox and adding it to the cbox container
    movable = new JCheckBox("Move on Drag");
    cbox.add(movable);

    //Actionlistener on the checkbox that recenters the
    //circle if the box is unchecked
    movable.addActionListener(new ActionListener()
        {public void actionPerformed(ActionEvent e) {repaint();}});

    //adding a MML with an anonymous class that gets the
    //cooordinates of the mouse if the mouse is being clicked
    //and dragged, and then it repaints the picture
    addMouseMotionListener(new MouseMotionListener()
        {
          public void mouseDragged(MouseEvent e) 
          {
            if(movable.isSelected())
            {
              x = e.getX() - (sizeX * 0.5);
              y = e.getY() - (sizeY * 0.5);
            }
            else
            {
              x = (getWidth() * 0.5) - (sizeX * 0.5);
              y = (getHeight() * 0.5) - (sizeY * 0.5);
            }
            repaint();
          }
          public void mouseMoved(MouseEvent e) {}
        });

    //setting size of the 
    //internal frame
    frame.setSize(400,500);
    frame.setVisible(true);
  }

  //paints the circle
  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);
    setBackground(Color.WHITE);
    g.setColor(Color.GREEN);

    //paints the circle in the middle if
    //the checkbox is deselected
    if(!movable.isSelected())
    {
      x = (getWidth() * 0.5) - (sizeX * 0.5);
      y = (getHeight() * 0.5) - (sizeY * 0.5);
    }

    sizeX = getWidth() * 0.3;
    sizeY = getHeight() * 0.3;
    g.fillOval((int)x,(int)y,(int)sizeX,(int)sizeY);
  }

  private double x;
  private double y;
  private double sizeX;
  private double sizeY;
  private JPanel cbox;
  private JCheckBox movable;
  private MouseMotionListener m;
  private JInternalFrame frame;
}
