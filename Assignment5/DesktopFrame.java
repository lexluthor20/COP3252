//Author: 

import javax.swing.JFrame;
import javax.swing.JDesktopPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenu;
import javax.swing.JInternalFrame;
import java.awt.Color;

public class DesktopFrame extends JFrame
{
  public DesktopFrame()
  {
    //creating the desktop pane
    window = new JDesktopPane();
    //adding the desktop pane to the DesktopFrame
    add(window);
    window.setBackground(Color.WHITE); 
    //creating the menu
    menuBar = new JMenuBar();
    create = new JMenu("Create");
    quit = new JMenu("Quit");


    //adding components to the menu
    menuBar.add(quit);
    menuBar.add(create);
    
    //creating the menu buttons
    pictureFrame = new JMenuItem("Picture Frame"); 
    movingPicture = new JMenuItem("Moving Picture Frame");
    quitProgram = new JMenuItem("Quit Program");

    //adding the dropdown menu buttons to the proper menu component
    quit.add(quitProgram);
    create.add(pictureFrame);
    create.add(movingPicture);

    //adding the menuBar to the DesktopFrame
    setJMenuBar(menuBar);
 
    //listens to see if the Quit Program button is pressed
    //creates an anonymous class ending the program if quitProgram
    //JMenuItem is pressed
    quitProgram.addActionListener(new ActionListener()
      {public void actionPerformed(ActionEvent e) {System.exit(1);}});

    //listens to see if the Picture Frame button is pressed
    //creates anonymous class that creates a new JInternalFrame
    //and adds a PictureFrame to the JInternalFrame
    pictureFrame.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          JInternalFrame frame = new JInternalFrame("Picture Frame",
                                                      true,true,true,true);
          window.add(frame);

          pf = new PictureFrame();
          frame.add(pf);

          frame.setSize(400,500);
          frame.setVisible(true);
        }

      });

    //listens to see if the moving picture button is pressed
    //creates an anonymous class that creates a new MovingPicture object
    movingPicture.addActionListener(new ActionListener()
      {public void actionPerformed(ActionEvent e) {mp = new MovingPicture(window);}});
  }
      
  //the window that the proj runs in
  public JDesktopPane window;

  //menu bar
  private JMenuBar menuBar;
  
  //quit and create buttons
  private JMenu quit;
  private JMenu create;
  
  //create button and its choices
  private JMenuItem pictureFrame;
  private JMenuItem movingPicture; 
  private JMenuItem quitProgram;
  
  //instantiations of pictureframe and moving picture
  private PictureFrame pf;
  private  MovingPicture mp;
}
