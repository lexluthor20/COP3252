//name : XXXXXXXXX

import java.util.Scanner;
import java.util.Random;

public class DiceStats 
{
    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);		//get keyboard input
        int num_dice,num_rolls,rand_value=0;		
        Random rand = new Random();			//create a rand object
        double percentage,numerator,denominator;
        
        System.out.print("How many dice will constitute one roll? ");
        num_dice = input.nextInt();			//get user input
        System.out.print("How many rolls? ");
        num_rolls = input.nextInt();			//get user input
        
        //creating an array size that is six times
	//larger than the number of dice (a counter
	//for each possible sum)
        int[] num_counters = new int[num_dice * 6];
        
        for(int i = 0; i < num_rolls; i++)
        {
          for(int j = 0; j < num_dice; j++)
            rand_value += rand.nextInt(6);		//adding each roll to sum
          
          num_counters[rand_value]++;			//incrementing frequency
          rand_value = 0;				//resetting the sum
        }       
        
        System.out.print("Sum\t# of times\tPercentage\n");
        
        for(int i = 0; i < num_counters.length-(num_dice-1); i++)
        {
	    //next three lines convert ints to doubles
            numerator = num_counters[i];
            denominator = num_rolls;
            percentage = 100 * (numerator/denominator);
	    //printing out statistics
            System.out.printf("%d\t%d\t\t%.2f %%\n",i+num_dice,num_counters[i],percentage);
        }

    }
    
}
