//Alex Dupper

//getting scanner tool
import java.util.Scanner;

public class Reverse 
{

    public static void main(String[] args) 
    {
        Scanner input = new Scanner(System.in);  //allowing keyboard input
        long num = 1, num_copy,answer=0;
        
        while(num != 0)				 //loop that stops if num=0
        {   
            int i = 0;
            System.out.print("\nPlease enter a long integer (0 to quit) : ");
            num = input.nextLong();
            if(num == 0)
                break;
            else
            {
                num_copy = num;			//creating a copy to work on
                
                while(num_copy > 0)
                {
                    answer += num_copy%10;  	//save last digit of num to new num
                    num_copy /= 10;           	//remove last digit of num
		    if(num_copy > 0)
		      answer *= 10;		//adding another digit place
                }
            }
            
            System.out.printf("\nThe number in reverse is: %d\n",answer);
            answer = 0;				//clearing the answer
        }

	System.out.print("\nGoodbye!");

    }
}
