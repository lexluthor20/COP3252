//Alex Dupper

import java.util.Scanner;

public class Pi 
{
    public static void main(String[] args) 
    {
        //numerator is always 4, denom starts at 1
        double numerator = 4.0,denominator = 1.0,answer=0;
        int size;
        
        //creating an input object
        Scanner input = new Scanner(System.in);
        
        //asking user for # of terms
        System.out.print("Compute to how many terms of the series? ");
        
        //user entering # of terms
        size = input.nextInt();
        
        
        System.out.print("terms\tPI approximation\n");
        
        //for loop getting the denominator values
        for(int i = 0; i < size; i++)
        {
	    //updating answer
            answer = answer + (numerator/denominator);
	    //printing out answer
            System.out.printf("%d\t%f\n", i+1,answer);
            denominator += 2;
            i++;
            if(i < size)
            {   //updating answer
                answer = answer - (numerator/denominator);
                denominator += 2;
		//printing out answer
                System.out.printf("%d\t%f\n", i+1,answer);
            }
        }
        
    }
    
}
