//XXXXXXX

class Board implements Cloneable
{
  //Board constructor setting
  //all spots to empty(' ')
  public Board()
  {
    for(int i = 1; i <= GetSize(); i++)
      SetValue(' ', i);
  }

  //overriding the clone function
  //in the cloneable interface
  public void clone(Board b)
  {
    int position = 1;
   
    while(position <= GetSize())
      SetValue(b.GetValue(position), position++);
  }

  //prints board
  public void PrintBoard()
  {
    int position = 1; 
    int num_lines = 0;
    System.out.print("\nGame Board:\tPositions:\n");
    
    for(int i = 1; i <= GetSize(); i++)
    {
      if(i > 2 && i < 8)  
        System.out.print("-----------\t-----------");
      
      System.out.printf("\n %c | %c | %c \t %d | %d | %d \n", 
		      GetValue(i), GetValue(++i), GetValue(++i),position++,position++,position++);
    
      num_lines++; 
    }

  }

  //sets the char in the array if  within array bounds and 
  //the spot has not already been filled. Returns false otherwise
  public void SetValue(char c, int position)
  {
    int i = -1;
    int j = -1;

    switch(position)
    {
      case(1):
          i = j = 0;
          break;
      case(2):
          i = 0;
          j = 1;
          break;
      case(3):
          i = 0; 
          j = 2; 
          break;
      case(4):
          i = 1;
          j = 0;
          break;
      case(5):
          i = j = 1;
          break;
      case(6):
          i = 1;
          j = 2;
          break;
      case(7):
          i = 2;
          j = 0;
          break;
      case(8):
          i = 2;
          j = 1;
          break;
      case(9):
          i = j = 2;
          break;
    }

    board[i][j] = c;
  }

  //allows other classes to view the value in each spot
  public char GetValue(int position)
  {
    char c = ' ';

    switch(position)
    {
      case(1):
      c = board[0][0];
      break;
      
      case(2):
      c = board[0][1];
      break;

      case(3):
      c = board[0][2];
      break;

      case(4):
      c = board[1][0];
      break;

      case(5):
      c = board[1][1];
      break;

      case(6):
      c = board[1][2];
      break;

      case(7):
      c = board[2][0];
      break;

      case(8):
      c = board[2][1];
      break;

      case(9):
      c = board[2][2];
      break;
    };
    
    return c;

  }

  //checks if spot is empty
  public boolean IsEmpty(int position)
  {return (GetValue(position) == ' ');}

  //Checks for win
  public boolean CheckWin(char val)
  {
    if(GetValue(1) == val && GetValue(4) == val && GetValue(7) == val)
      return true;
    else if(GetValue(2) == val && GetValue(5) == val && GetValue(8) == val)
      return true;
    else if(GetValue(3) == val && GetValue(6) == val && GetValue(9) == val)
      return true;
    else if(GetValue(1) == val && GetValue(2) == val && GetValue(3) == val)
      return true;
    else if(GetValue(4) == val && GetValue(5) == val && GetValue(6) == val)
      return true;
    else if(GetValue(7) == val && GetValue(8) == val && GetValue(9) == val)
      return true;
    else if(GetValue(1) == val && GetValue(5) == val && GetValue(9) == val)
        return true;
    else if(GetValue(3) == val && GetValue(5) == val && GetValue(7) == val)
        return true; 
    else
      return false;
  }
  
  //user interface to get the board size
  public final int GetSize()
  {return TOTAL_SIZE;}

  //total size
  private final int TOTAL_SIZE = 9;
  private char[][] board = new char[3][3];
}
