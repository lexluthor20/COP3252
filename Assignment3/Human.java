import java.util.Scanner;

class Human
{
  //constructor, saves the token the player
  //will be using and if they go first or second
  //(only important for Advanced players)
  public Human(char t)
  {token = t;}

  public char GetToken()
  {return token;}

  public void Play(Board b, Human other_player)
  {
    int temp = 0;
    
    System.out.print("please enter a move(1-9): ");
    temp = in.nextInt();

    //for loop that runs until a correct
    //value is entered
    while((temp < 1) || (temp > b.GetSize()))
    {
      System.out.print("Error: Bad input, please reenter: ");
      temp = in.nextInt();
    }

    //runs until user enters an empty spot
    while(b.IsEmpty(temp) == false)
    {
      System.out.print("Error: Spot taken, please try another: ");
      temp = in.nextInt();
    }
    
    b.SetValue(token,temp);
  }

  protected char token;
  protected Scanner in = new Scanner(System.in);
}
