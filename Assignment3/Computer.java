//XXXXXXXXXX

import java.util.Random;

class Computer extends Human
{
  
  public Computer(char t)
  {super(t);}
 

  public void Play(Board b, Human other_player)
  {
    boolean found_spot = false;

      
    //for loop that tries placing
    //a token on every spot to get a win
    for(int itr = 1; itr <= b.GetSize(); itr++)
    {
      //cloning the game board
      Board temp_board = new Board(); 
      temp_board.clone(b);   

      //placing a token on the temporary board
      //if it results in a win
      if(temp_board.IsEmpty(itr))
      {
        temp_board.SetValue(token,itr);

        if(temp_board.CheckWin(token))
        {
          b.SetValue(token,itr);
          found_spot = true;
          break;
        }
      }
    } 
        
    //placing a token on the temporary board
    //if it stops other_player from winning
    for(int itr = 1; itr <= b.GetSize() && found_spot == false; itr++) 
    {

      //cloning the game board
      Board temp_board = new Board(); 
      temp_board.clone(b);   

      if(temp_board.IsEmpty(itr))
      { 
        temp_board.SetValue(other_player.GetToken(),itr);
           
        if(temp_board.CheckWin(other_player.GetToken()))
        {
          b.SetValue(token,itr);
          found_spot = true;
          break;
        }
      }
    }
      
    //this code block runs if a winning
    //spot or preventing an other_player
    //win could not be found
    if(found_spot == false)
    {
      //places on middle spot if empty
      if(b.IsEmpty(5))
        b.SetValue(token,5);
      //places randomly as a last option 
      else
      {
        int rand_spot;
        Random rand = new Random();
        rand_spot = rand.nextInt(b.GetSize()) + 1;
        
        while(b.IsEmpty(rand_spot) == false)
          rand_spot = rand.nextInt(b.GetSize()) + 1;
        
        b.SetValue(token,rand_spot);

      }
    } 
  }


}
