import java.util.Scanner;

class TicTacToe
{

  //determining if player is human or computer
  public static void main(String[] args)
  {
    int turn_num = 0;

    //prints out empty board
    game_board.PrintBoard();

    //creates two humans if no args passed
    if(args.length == 0)
    {
      player1 = new Human('X');
      player2 = new Human('O');
    }

    //creates two computers if "-c" passed in     
    else if(args.length == 1 && args[0].equals("-c"))
    {
      player1 = new Computer('X');
      player2 = new Computer('O');
    }

    //creates a computer, then a human if "-c 1" passed in
    else if(args.length == 2 && args[0].equals("-c") && args[1].equals("1"))
    {
      player1 = new Computer('X');
      player2 = new Human('O');
    }

    //creates a human, then a computer if "-c 2" passed in
    else if(args.length == 2 && args[0].equals("-c") && args[1].equals("2"))
    {
      player1 = new Human('X');
      player2 = new Computer('O');
    }

    else
    {
      System.out.println("Usage: java TicTacToe [-c [1|2]]");
      System.exit(1);
    }

    //main game loop
    while(turn_num < 9)
    {
      //player1 goes if it is an
      //even turn number(0,2,4,...)
      if(turn_num % 2 == 0)
        player1.Play(game_board,player2);
      
      //player2 goes on the odds
      else
        player2.Play(game_board,player1);

      game_board.PrintBoard();

      //Checks to see if the player who
      //just went won
      if(game_board.CheckWin(player1.GetToken()))
      {
	      System.out.print("player1 won!\n");
        break;
	    }
      if(game_board.CheckWin(player2.GetToken()))
      {
	      System.out.print("player2 won!\n");
        break;
	    }
      //lets the next player go
      turn_num++;
    }
  }
 
    //creating two players and a game board and an input object and a turn counter
    private static Human player1;
    private static Human player2;
    private static Board game_board = new Board();
    private static Scanner input = new Scanner(System.in); 
}
