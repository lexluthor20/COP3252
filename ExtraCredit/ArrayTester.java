//pretty much just translated what
//you asked for in the writeup into
//java code
import java.util.Arrays;

public class ArrayTester<T>
{
  public ArrayTester(T[] arr, CustomTest<T> test)
  {
    array = arr;
    tester = test;

    Arrays.sort(array);
  }

  public void printIfValid()
  {
    for(int i = 0; i < array.length; i++)
    {
      if(tester.test(array[i]) == true)
      {
        System.out.print(array[i]);
        System.out.print(' ');
      }
    }
    System.out.println();
  }

  public int countIfValid()
  {
    int counter = 0;

    for(int i = 0; i < array.length; i++)
    {
      if(tester.test(array[i]) == true)
        counter++;
    }
    return counter;
  }

  CustomTest<T> tester;
  T[] array;
}
