public class GreaterThan<T extends Comparable<T>> implements CustomTest<T>
{
    public GreaterThan(T t)
    {
      data = t;
    }

    public boolean test(T t)
    {
      if(data.compareTo(t) < 0)
        return true;
      else
        return false;
    }

  private T data;
}
