public class AllLower implements CustomTest<String>
{
  //copies old string, converts it all to
  //lowercase then compares it to the original
  public boolean test(String s1)
  {
    String s2 = new String(s1.toLowerCase());

    if(s1.compareTo(s2) == 0)
      return true;
    else
      return false;
  }
}
