public class IsEven<T extends Number> implements CustomTest<T>
{
  public boolean test(T t)
  {
    //must use intValue() method or it doesn't work
    //since modulus only works for integers
    if(t.intValue() % 2 == 0)
      return true;
    else
      return false;
  }
}

