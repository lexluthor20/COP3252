////Assignment4: File Viewer

import java.io.*;
import java.util.*;
import java.nio.file.Path;
import java.text.SimpleDateFormat;

class FileViewer
{
  public static void main(String args[])
  {
    File file1;
    File file2;

    if(args.length == 0)
    {
      file1 = new File("./");
      DirInfo(file1.getAbsoluteFile());
    } 
    else if(args[0].equals("-i"))
    {
      if(args.length != 2)
        PrintError();

      else
      {
        file1 = new File(args[1]);
        FileInfo(file1);	
      }
    }
    else if(args[0].equals("-v"))
    { 
      if(args.length != 2)
        PrintError();

      else
      {
        file1 = new File(args[1]);
        FileView(file1);
      }
    } 
    else if(args[0].equals("-c"))
    {
      if(args.length != 3)
        PrintError();

      else
      {
        file1 = new File(args[1]);
        file2 = new File(args[2]);

        if(file2.exists())
          System.out.println("File " + file2.getName() + "  already exists.");
        else
          FileCopy(file1,file2);

      }

    } 
    else
      PrintError();

  }

  //print out a correct usage message if the input was invalid 
  private static void PrintError()
  {
      System.out.print("Usage: java -jar hw4.jar [-i [<file>|<directory>]");
      System.out.println("|-v <file>|-c <sourceFile> <destFile>]");
  }

  //prints out the full pathname, execution permission, and
  //file size (roughly similar to the -l flag in UNIX for the ls command)
  private static void FileInfo(File f)
  {
    if(f.isFile())
    {
      System.out.println("File Path: " + f.getAbsolutePath());
      System.out.println("Is executable? " + f.canExecute());
      System.out.println("Size: " + f.length() + " bytes");
      
      Date d = new Date(f.lastModified());
      String str = d.toString();
      str = str.substring(0,19);

      System.out.println("Last Modified Date: " + str);
    }
 
    else if(f.isDirectory())
    {
      DirInfo(f);
    }
    else
    {
      System.out.println("Error: Invalid File");
    }
 
  }

  //printing out the byte size and name of all files
  //in a directory (similar to the ls command in UNIX)
  private static void DirInfo(File dir)
  {
    File[] f = dir.listFiles();

    //The anonymous class below is modeled after the example found at:
    //http://www.cs.fsu.edu/~myers/cop3252/notes/examples/sorting/SortingAnon.java
    Arrays.sort(f, new Comparator<File>()
        {
          public int compare(File f1, File f2)
          {
            return((int)f1.length() - (int)f2.length());
          }
        });
    //end of similarly modeled code

    System.out.println("Size  Filename\n");
    
    for(File itr : f)
    {
      System.out.println(itr.length() + "  " + itr.getName());
    }

  }

  //printing out the file (similar to the cat command in UNIX)
  private static void FileView(File f)
  {
    try 
    {
      //creating the input stream
      BufferedReader input = new BufferedReader(new FileReader(f));

      //printing out each character until the buffer is empty
      while(input.ready())
        System.out.print((char)input.read());
      
      input.close();
    }
    catch(IOException ioe) 
    {
      System.out.println("Error: Invalid File");
    }

  }

  //Copies the data from the src file into the dst file.
  //Similar to the cp command in UNIX
  private static void FileCopy(File src, File dst)
  {
    try
    {
      //creating the new file
      dst.createNewFile();
      
      //creating the input and output streams
      FileWriter output = new FileWriter(dst);
      BufferedReader input = new BufferedReader(new FileReader(src));
      
      //appending the input stream to the new file
      while(input.ready())
         output.append((char)input.read());

      input.close();
      output.close();
    }
    catch(IOException ioe)
    {
      System.out.println("Error occured writing file");
    }

  }
}
