import java.util.Collections;
import java.util.Arrays;
import java.io.Serializable;

public class Deck implements Serializable
{
  public Deck()
  {
    int card_num = 0;
    
    cardDeck = new Card[52];

    //creating a 52 card deck
    for(int i = 0; i < 4; i++)
    {
      for(int j = 0; j < 13; j++)
      {
        if(i == 0) {cardDeck[card_num++] = new Card("spades",j+2);}
        else if(i == 1) {cardDeck[card_num++] = new Card("hearts",j+2);}
        else if(i == 2) {cardDeck[card_num++] = new Card("diamonds",j+2);}
        else {cardDeck[card_num++] = new Card("clubs",j+2);}
      }
    }

    cards_in_deck = 52;
    next_card = 0;
  }

  public int CardsLeft()
  {
    return cards_in_deck;
  }

  public boolean Empty()
  {
    if(cards_in_deck > 0)
      return false;
    else
      return true;
  }

  public void Reshuffle() 
  {
    cards_in_deck = 52;
    next_card = 0;

    Collections.shuffle(Arrays.asList(cardDeck));
  }

  public void Print()
  {
    while(!Empty())
      System.out.println(GetNextCard().PrintCard());
  } 

  public Card GetNextCard()
  {
    cards_in_deck--;
    return cardDeck[next_card++];
  }

  private Card[] cardDeck;
  private int cards_in_deck;
  private int next_card;
}
