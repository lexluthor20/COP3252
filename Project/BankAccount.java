import javax.swing.ImageIcon;
import java.io.Serializable;

public class BankAccount implements Serializable
{
  public BankAccount(int t)
  {
    total = t; 
  }

  public void Deposit(int value)
  {
    total += value;
  }

  public void Withdrawl(int value)
  {
    if(value > total)
      total = 0;

    else if (value < 0)
      return;
    
    else
      total -= value;
  }

  //used to transfer money from the
  //pot to a player or from a player
  //to the pot for winning 
  public void Transfer(BankAccount b)
  {
    Deposit(b.GetValue());
    b.Withdrawl(b.GetValue());
  }

  public int GetValue()
  {
    return total;
  }

  private int total; 
}
