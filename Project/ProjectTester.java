import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.lang.ClassNotFoundException;

public class ProjectTester
{
  public static void main (String args[])
  {
    newOrLoad = new JFrame();
    panel = new JPanel();
    loadGame = new JButton("Load Game");
    newGame = new JButton("New Game");
    panel.add(newGame);
    panel.add(loadGame);
    newOrLoad.add(panel);
    newOrLoad.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    newOrLoad.setSize(300,300);
    newOrLoad.setVisible(true);

    //creates a new game if it is chosen
    newGame.addActionListener(new ActionListener()
        {public void actionPerformed(ActionEvent e)  {CreateGame();}});

    //tried to add a load game functionality to the program... Didn't work
    //so this will just make a new game no matter which button is pressed
    loadGame.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            try
            {
              newOrLoad.setVisible(false);
              FileInputStream fis = new FileInputStream("gamedata");
              ObjectInputStream ois = new ObjectInputStream(fis);

              c = (Casino) ois.readObject();
            }
            //if exceptions are found, a new game is generated
            catch(IOException ioe) {CreateGame();}
            catch(ClassNotFoundException cnfe) {CreateGame();}
          }
        });
  }

  public static void CreateGame()
  {
    newOrLoad.setVisible(false);
    c = new Casino();
    c.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    c.setSize(1010,800); //set frame size

    //tried to add a save game functionality to the program... Didn't work
    c.saveGame.addActionListener(new ActionListener()
      {
        public void actionPerformed(ActionEvent e)
        {
          try
          {
            String filename = new String("gamedata");
            FileOutputStream fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            //writes the casino to the file
            oos.writeObject(c);

            //closes the file
            oos.close();
          }
          catch(FileNotFoundException filenf)
          {
            System.out.println("Error: File not found");
          }
          catch(IOException ioe)
          {
            System.out.println("Error writing file");
          }
        }
      });

  }

  //private static Casino c;
  private static Casino c;
  private static JFrame newOrLoad;
  private static JPanel panel;
  private static JButton newGame;
  private static JButton loadGame;
}
