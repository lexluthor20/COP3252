import java.io.Serializable;

public class Dealer implements Serializable
{
  public Dealer()
  {
    deck = new Deck();

    //shuffles the deck 5 times
    for(int i = 0; i < 5; i++)
      deck.Reshuffle();
  }

  public Card DealCard()
  {
    if(!deck.Empty())
      return deck.GetNextCard();

    else
    {
      deck.Reshuffle();
      return deck.GetNextCard();
    }
  }

  public void ShuffleDeck()
  {
    for(int i = 0; i < 5; i++)
      deck.Reshuffle();
  }

  private Deck deck;
}
