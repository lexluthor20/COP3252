import javax.swing.ImageIcon;
import java.util.Arrays;
import java.util.Comparator;
import java.io.Serializable;

public class CardHand implements Serializable
{
  //saves the hands given to the player into
  //member data
  public CardHand(Card[] cards)
  {
    hand = new Card[5];

    for(int i = 0; i < hand.length; i++)
      hand[i] = cards[i];
  }

  //helpful for throwing away cards and
  //getting new ones
  public void SetCard(Card c, int i)
  {
    if(i < 0)
      hand[0] = c;
    else if(i > 4)
      hand[4] = c;
    else
      hand[i] = c;
  }

  public String PrintCard(int i)
  {
    return hand[i].PrintCard();
  }

  public int GetHandSize()
  {
    return hand.length;
  }

  public int GetHandRanking()
  {
    return hand_ranking;
  }

  public ImageIcon GetHiddenImage(int i)
  {
    return hand[i].GetHiddenImage();
  }

  public ImageIcon GetImage(int i)
  {
    return hand[i].GetImage();
  }

  //function that checks every hand rank
  //from highest to lowest rank
  public String FindHandRanking()
  {
    Sort();

    if(RoyalFlush())
      return "You Have a: Royal Flush";

    else if(StraightFlush())
      return "You Have a: Straight Flush";

    else if(FourOfAKind())
      return "You Have a: Four of a Kind";

    else if(FullHouse())
      return "You Have a: Full House";

    else if(Flush())
      return "You Have a: Flush";

    else if(StraightHand())
      return "You Have a: Straight";

    else if(ThreeOfAKind())
      return "You Have a: Three of a Kind";

    else if(TwoPair())
      return "You Have a: Two Pair";

    else if(OnePair())
      return "You Have a: One Pair";

    else
    {
      hand_ranking = 100 + hand[0].GetValue(); 
      return "You Have a: High Card";
    }
      
  }

  public void Sort()
  {
    Arrays.sort(hand, new Comparator<Card>()
        {
          public int compare(Card c1, Card c2)
          {
            return c2.GetValue() - c1.GetValue();
          }
        });
  }

  //helper function
  public int Straight()
  {
    if(hand[0].GetValue() == hand[1].GetValue() + 1 &&
        hand[1].GetValue() == hand[2].GetValue() + 1 &&
        hand[2].GetValue() == hand[3].GetValue() + 1 &&
        hand[3].GetValue() == hand[4].GetValue() + 1) 
    {
      return hand[0].GetValue();
    }

    else
    {
      return -1;
    }
  }

  //helper function
  public int SameSuit()
  {
    if(hand[0].GetSuit().equals(hand[1].GetSuit()) &&
        hand[1].GetSuit().equals(hand[2].GetSuit()) &&
        hand[2].GetSuit().equals(hand[3].GetSuit()) &&
        hand[3].GetSuit().equals(hand[4].GetSuit())) 
    {
      return hand[0].GetValue();
    }

    else
    {
      return -1;
    }

  }
  //checks that all cards are the
  //same suit and in consecutive order
  //and the first card is an Ace
  public boolean RoyalFlush()
  {
    if(Straight() == 14 && SameSuit() != -1)
    {
      hand_ranking = 1000;
      return true;
    }
    else
      return false;
  }

  //if cards are in consecutive order,
  //and the same suit
  public boolean StraightFlush()
  {
    if(Straight() != -1 && SameSuit() != -1)
    {
      hand_ranking = 900 + Straight();
      return true;
    }
    else
      return false;
  }

  //if cards are in consecutive order
  public boolean StraightHand()
  {
    if(Straight() != -1)
    {
      hand_ranking = 500 + Straight();
      return true;
    }
    else
      return false;
  }

  //if cards are the same suit
  public boolean Flush()
  {
    if(SameSuit() != -1)
    {
      hand_ranking = 600 + SameSuit();
      return true;
    }
    return
      false;
  }

  //counts the number of cards
  //with the same value. True if
  //counter is 4
  public boolean FourOfAKind()
  {
    int counter;

    for(int i = 0; i < 5; i++)
    {
      counter = 0;

      for(int j = 0; j < 5; j++)
      {
        if(hand[i].GetValue() == hand[j].GetValue())
          counter++;
      }

      if(counter == 4)
      {
        hand_ranking = 800 + hand[i].GetValue();
        return true;
      }
    }

    return false;
  }

  //two iterators on both sides
  //of the hand of cards. If
  //the two counters add up to 5
  //returns true
  public boolean FullHouse()
  {
    int counter1 = 0;
    int counter2 = 0;
    int value = 0;

    for(int i = 0; i < 3; i++)
    {
      counter1 = 0;

      for(int j = 0; j < 3; j++)
      {
        if(hand[i].GetValue() == hand[j].GetValue())
          counter1++;
      }
      if(counter1 == 3 || counter1 == 2)
      {
        value = hand[i].GetValue(); 
        break;
      }
    }

    for(int k = 4; k >= 2; k--)
    {
      counter2 = 0;

      for(int m = 4; m >= 2; m--)
      {
        if(hand[k].GetValue() == hand[m].GetValue())
          counter2++;
      }
      if((counter1 == 3 && counter2 == 2) || (counter1 == 2 && counter2 == 3))
      {
        if(counter1 == 3) 
        {
          hand_ranking = 700 + value;
        }
        else
        {
          hand_ranking = 700 + hand[k].GetValue();
        }
        return true;
      }

    }

    return false;
  }

  //iterates through the hand and
  //returns true if the counter that
  //counts same card value is 3
  public boolean ThreeOfAKind()
  {
    int counter;

    for(int i = 0; i < 5; i++)
    {
      counter = 0;

      for(int j = 0; j < 5; j++)
      {
        if(hand[i].GetValue() == hand[j].GetValue())
          counter++;
      }

      if(counter == 3)
      {
        hand_ranking = 400 + hand[i].GetValue();
        return true;
      }
    }

    return false;
  }

  //similar to the full house function,
  //returns true if the two counters add
  //up to 4 and values ard different
  public boolean TwoPair()
  {
    int counter1 = 0;
    int counter2 = 0;
    int value = 0;

    for(int i = 0; i < 3; i++)
    {
      counter1 = 0;

      for(int j = 0; j < 3; j++)
      {
        if(hand[i].GetValue() == hand[j].GetValue())
          counter1++;
      }
      if(counter1 == 2)
      {
        value = hand[i].GetValue(); 
        break;
      }
    }

    for(int k = 4; k >= 2; k--)
    {
      counter2 = 0;

      for(int m = 4; m >= 2; m--)
      {
        if(hand[k].GetValue() == hand[m].GetValue())
          counter2++;
      }
      if(counter1 == 2 && counter2 == 2)
      {
        if(value > hand[k].GetValue()) 
        {
          hand_ranking = 300 + value;
        }
        else
        {
          hand_ranking = 300 + hand[k].GetValue();
        }
        return true;
      }

    }

    return false;
  }

  //iterates through hand, returns
  //true if two cards of same value
  //are found
  public boolean OnePair()
  {
    int counter;

    for(int i = 0; i < 5; i++)
    {
      counter = 0;

      for(int j = 0; j < 5; j++)
      {
        if(hand[i].GetValue() == hand[j].GetValue())
          counter++;
      }

      if(counter == 2)
      {
        hand_ranking = 200 + hand[i].GetValue();
        return true;
      }
    }

    return false;
  }

  private Card[] hand;

  //value associated to players cards,
  //allows easy ranking of player's hands
  //when trying to determine the winner
  private int hand_ranking;
}
