import java.util.Arrays;
import java.util.Comparator;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JDesktopPane;
import javax.swing.JTabbedPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JOptionPane;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JInternalFrame;
import javax.swing.JSlider;
import javax.swing.JPanel;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.BorderLayout;
import java.util.Scanner;
import java.io.Serializable;

public class Casino extends JFrame implements Serializable
{
  public Casino()
  {
    //adding the JTabedPane to the Casino 
    window = new JTabbedPane();
    add(window);

    pot = new BankAccount(0);
    showdownCounter = 0;
    cardDealer = new Dealer();

    
    //creates the num of players selection
    playerNumFrame = new JFrame();
    playerNumButton = new JButton("Submit");
    playerNumLabel = new JLabel("Please Select Number of Players");
    playerNumRadio = new JRadioButton[5];
    playerNumGroup = new ButtonGroup();
    playerNumBox = Box.createHorizontalBox();
    
    for(int i = 0; i < playerNumRadio.length; i++)
    {
      if(i == 0)
        playerNumRadio[i] = new JRadioButton("" + (i+2), true);
      else
        playerNumRadio[i] = new JRadioButton("" + (i+2), false);

      playerNumRadio[i].setVisible(true);
      playerNumBox.add(playerNumRadio[i]);
      playerNumGroup.add(playerNumRadio[i]);
    }

    playerNumLabel.setVisible(true);
    playerNumFrame.setLayout(new BorderLayout());
    playerNumFrame.add(playerNumLabel, BorderLayout.NORTH);
    playerNumFrame.add(playerNumBox, BorderLayout.CENTER);
    playerNumFrame.add(playerNumButton, BorderLayout.SOUTH);
    playerNumFrame.setSize(300,200);
    playerNumFrame.setVisible(false);

    //creates the ante chooser
    anteSlider = new JSlider(2,128,2);
    anteCommit = new JButton("Ante is 2 Dollars");;
    antePanel = new JPanel();
    anteFrame = new JFrame();

    antePanel.add(anteSlider);
    antePanel.add(anteCommit);
    anteFrame.add(antePanel);
    anteFrame.setSize(300,200);
    anteFrame.setVisible(true);

    //runs while user plays with the slider
    anteSlider.addChangeListener(new ChangeListener()
        {
          public void stateChanged(ChangeEvent e)
          {
            anteCommit.setText("Ante is " + anteSlider.getValue() + " Dollars");
          }
        });

    //sets ante to chosen value, sets the num of players frame to visible
    anteCommit.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            ANTE = anteSlider.getValue();
            betMin = ANTE;
            anteFrame.setVisible(false);
            playerNumFrame.setVisible(true);
          }
        });


    //creates part of the player's names optionPane 
    playerNameFrame = new JFrame();
    playerNameInput = new JOptionPane();

    playerNameFrame.add(playerNameInput);
    playerNameFrame.add(playerNameInput);
    playerNameFrame.setVisible(false);

    
    playerNumButton.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            for(int i = 0; i < playerNumRadio.length; i++)
            {
              if(playerNumRadio[i].isSelected())
                numPlayers = i+2;
            }
            //creates the dynamic portion of the 
            //player name optionPane
            playerNumFrame.setVisible(false);
            players = new Player[numPlayers]; 
            dealtCards = new Card[numPlayers][5];
            player_name = new String[numPlayers];


            //gets the player's names
            for(int i = 0; i < numPlayers; i++)
            {
              player_name[i] = (String)playerNameInput.showInputDialog 
                                        ("Please Enter Player " + (i+1) + "'s Name");
            }
            CreatePlayers(player_name);
            setVisible(true);
          }
        });

    //creating the menu
    menuBar = new JMenuBar();
    leaveGame = new JMenu("Leave Game");
    potMenu = new JMenu("Pot is currently: $" + pot.GetValue());
    betMenu = new JMenu("Required Bet: $" + betMin);

    //adding components to the menu
    menuBar.add(leaveGame);
    menuBar.add(potMenu);
    menuBar.add(betMenu);
    
    //creating the menu buttons
    saveGame = new JMenuItem("Save Game");
    quitGame = new JMenuItem("Quit Game");
    
    //adding the dropdown menu buttons to the proper menu component
    leaveGame.add(saveGame);
    leaveGame.add(quitGame);

    //adding the menuBar to the Casino
    setJMenuBar(menuBar);
 
    //quits the game if user hits 'quit game' button
    quitGame.addActionListener(new ActionListener()
      {public void actionPerformed(ActionEvent e)  {System.exit(1);}});

    replayOption = new JOptionPane();
    replayFrame = new JFrame();
    replay = JOptionPane.YES_OPTION;
    
  }

  public void CreatePlayers(String playerName[])
  {
    //dealing the player's their cards.
    for(int j = 0; j < 5; j++)
    {
      for(int i = 0; i < numPlayers; i++)
      {
        dealtCards[i][j] = cardDealer.DealCard(); 
      }
    }

    //creating a new player object
    for(int i = 0; i < numPlayers; i++)
    {
      //System.out.print("Player's Name: ");
      players[i] = new Player(playerName[i], 500, dealtCards[i],this);
      window.addTab(playerName[i],players[i]);
    }
  }

  public int GetNumPlayers()
  {
    return players.length;
  }

  public String GetPlayersName(int index)
  {
    if(index < 0)
      return players[0].GetName();
    else if(index >= players.length)
      return players[players.length-1].GetName();
    else
      return players[index].GetName();
  }
  
  public void SortWinners()
  {
    Arrays.sort(players, new Comparator<Player>()
        {
          public int compare(Player p1, Player p2)
          {
            return p2.GetHandRanking() - p1.GetHandRanking();
          }
        });
  }     
   
  //the window that the proj runs in
  //public JDesktopPane window;
  public JTabbedPane window;
  //menu bar
  private JMenuBar menuBar;
  
  //quit and create buttons
  private JMenu leaveGame;
  public JMenu potMenu;
  public JMenu betMenu;
  
  //create button and its choices
  public JMenuItem saveGame;
  private JMenuItem quitGame; 

  public int waitCounter;
  public int showdownCounter;

  //needed for the creation of
  //players
  public Player[] players;
  private Card[][] dealtCards; 

  //gets number of players
  private JFrame playerNumFrame;
  private ButtonGroup playerNumGroup;
  private Box playerNumBox;
  private JRadioButton[] playerNumRadio;
  private JLabel playerNumLabel;
  private JButton playerNumButton;
  private int numPlayers;

  //gets player's names
  private String player_name [];
  private JFrame playerNameFrame;
  private JOptionPane playerNameInput;

  //dealer of the casino
  public Dealer cardDealer;

  //needed for the showdown frame
  public JOptionPane replayOption;
  public JFrame replayFrame;
  public int replay;

  //needed for info about
  //the table's pot
  public BankAccount pot;
  public int betMin;
  public int ANTE;
  private JSlider anteSlider;
  private JButton anteCommit;
  private JPanel antePanel;
  private JFrame anteFrame;
}
