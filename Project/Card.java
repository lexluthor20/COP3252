import java.lang.String;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class Card implements Serializable
{
  //sets suit and value to parameters,
  //then sets the facedown card to joker,
  //and faceup card to the corresponding
  //file name with the right suit and value
  public Card(String s, int v)
  {
    suit = s;
    value = v;

    faceup = new ImageIcon(getClass().getResource
        ("Card_Pictures/" + value + "_of_" + suit + ".png")); 

    facedown = new ImageIcon(getClass().getResource("Card_Pictures/facedown.png"));
  }
  
  public ImageIcon GetImage()
  {
    return faceup;
  }

  public ImageIcon GetHiddenImage()
  {
    return facedown;
  }

  public String GetSuit()
  {
    return suit;
  }
  
  public int GetValue()
  {
    return value;
  }

  //prints out facenames after
  //the value goes over 10
  public String PrintCard()
  {
     if(value < 11)
       return value + " of " + suit;
     else if(value == 11)
       return "Jack of " + suit;
     else if(value == 12)
       return "Queen of " + suit;
     else if(value == 13)
       return "King of " + suit;
     else
       return "Ace of " + suit;
  }
  private String suit;
  private int value;
  private transient ImageIcon faceup;
  private transient ImageIcon facedown;
}
