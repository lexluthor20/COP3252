import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JSlider;
import java.awt.Color;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.Serializable;


public class Player extends JPanel implements Serializable
{
  public Player(String n, int money, Card[] dealtCards, Casino casino)
  {
    name = n;
    valueBetted = 0;
    showdown = false;
    timesRepeated = 0;
    raised = 0;
    folded = false;


    appColor = new Color(207,83,0); 
    setBackground(appColor);

    //creates a card hand of 5 cards
    cardPictures = new JLabel[5];

    //creates 5 check boxes to determine
    //which cards the player wants to
    //throw away
    throwAway = new JCheckBox[5];
    cardBoxes = new Box[5];

    //sets the entire layout as a border layout
    superLayout = new BorderLayout(); 
    setLayout(superLayout);

    //Creates a JPanel in the South portion of the window
    //to store the JLabels which hold the cards
    bottomOfPage = new JPanel();
    bottomOfPage.setBackground(appColor);
    add(bottomOfPage,superLayout.SOUTH);

    //sets the players hand of cards
    hand = new CardHand(dealtCards);


    //creates the images of the player's cards
    for(int i = 0; i < hand.GetHandSize(); i++)
    {
      cardPictures[i] = new JLabel(hand.GetHiddenImage(i));
      throwAway[i] = new JCheckBox("Throw Away",false);
      cardBoxes[i] = new Box(1);
      cardBoxes[i].add(throwAway[i]);
      cardBoxes[i].add(cardPictures[i]);
      throwAway[i].setVisible(false);
      bottomOfPage.add(cardBoxes[i]);
    }

    //creates a Grid Bag Layout in the Center Portion
    //of the Border Layout
    centerOfPage = new JPanel(new GridBagLayout());
    centerOfPage.setBackground(appColor);
    add(centerOfPage,superLayout.CENTER);
    constraints = new GridBagConstraints(); 


    //putting the Flip Cards Button in the correct Area
    constraints.ipady = 10;
    constraints.ipadx = 10;
    constraints.weightx = 2.0;
    constraints.weighty = 2.0;
    constraints.anchor = GridBagConstraints.SOUTHWEST;
    constraints.insets = new Insets(20,20,20,20);
    constraints.gridwidth = 2;
    constraints.gridx = 6;
    constraints.gridy = 1;

    //creating the Flip Cards Button
    flipCards = new JButton("Flip Cards");

    //Flips the cards when the Flip Cards Button is pressed
    flipCards.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e) 
          {
            //if statemet that allows for toggling of cards. Hides all
            //of the cards if PrintCards is called and the first icons
            //are the same
            if(cardPictures[0].getIcon().equals(hand.GetImage(0)))
              HideCards();
            else
              PrintCards();
          }
        });

    //adds the button to the Screen
    centerOfPage.add(flipCards,constraints);

    //putting the Best Hand Label in the correct area
    constraints.anchor = GridBagConstraints.NORTHEAST;
    constraints.ipady = 30;

    //creates the best hand label and adds it to the screen
    bestHand = new JLabel();
    centerOfPage.add(bestHand,constraints);
    bestHand.setForeground(Color.WHITE);
    bestHand.setVisible(true);

    //creates the player's wallet and adds the current value to the screen 
    wallet = new BankAccount(money);
    displayWallet = new JLabel("Wallet: $" + wallet.GetValue());
    displayWallet.setForeground(Color.WHITE);
    displayDifference = new JLabel("Wallet after you bet: $" + 
        (wallet.GetValue() - casino.betMin + raised));
    displayDifference.setForeground(Color.WHITE);
    walletBox = Box.createVerticalBox();
    walletBox.add(displayWallet);
    walletBox.add(displayDifference);
    add(walletBox,superLayout.NORTH);
    displayWallet.setVisible(true);
    displayDifference.setVisible(true);

    //creates the JSlider to change the bet amount and 
    //two buttons to either commit the bet or just check/match
    constraints.anchor = GridBagConstraints.CENTER;
    betAmount = new JSlider(0,wallet.GetValue());
    betAmount.setValue(0);
    betAmount.setPaintLabels(true);
    betAmount.setVisible(true);
    commitBet = new JButton("Raise " + betAmount.getValue() + " Dollars");
    foldButton = new JButton("Fold");
    foldButton.setVisible(true);

    //runs when "fold" is clicked
    foldButton.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
            folded = true;
            PlayerFolded();
            CheckForNextRound(casino);
          }
        });

    //runs while user plays with the slider
    betAmount.addChangeListener(new ChangeListener()
        {
          public void stateChanged(ChangeEvent e)
          {
            raised = betAmount.getValue();
            commitBet.setText("Raise " + (raised) + " Dollars");
            WalletFuture(casino);
          }
        });

    //runs when the user clicks the raise button
    commitBet.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e) 
          {
            BettingAlgorithm(casino);
            timesRepeated++;
            Waiting();
            CheckForNextRound(casino);
          }
        });

    //encapsulating all of the betting GUI elements
    betBox = Box.createVerticalBox();
    betBox.add(betAmount);
    betBox.add(commitBet);
    betBox.add(foldButton);
    centerOfPage.add(betBox,constraints);

    //creates the throw away cards button
    commitDraw = new JButton("Throw Away Cards"); 
    commitDraw.setVisible(false);
    centerOfPage.add(commitDraw,constraints);
    commitDraw.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {

            casino.waitCounter = 0;

            for(int i = 0; i < hand.GetHandSize(); i++)
            {
              if(throwAway[i].isSelected())
              {
                hand.SetCard(casino.cardDealer.DealCard(), i);
                bestHand.setText(hand.FindHandRanking());
                PrintCards();
              }
              throwAway[i].setSelected(false);
            }
            Waiting();
            showdown = true;

            for(int j = 0; j < casino.players.length; j++)
            {
              if(casino.players[j].waitOnOthers.isVisible())
                casino.waitCounter++;
            }
            if(casino.waitCounter == casino.players.length)
            {
              for(int k = 0; k < casino.players.length; k++)
              {
                if(casino.players[k].folded == false)
                  casino.players[k].BettingRound();
              }
            }
          }
        });
 
    waitOnOthers = new JLabel("Waiting on other Players");
    waitOnOthers.setVisible(false);
    centerOfPage.add(waitOnOthers,constraints);

    //gets the player's best hand
    bestHand.setText(hand.FindHandRanking());
  }

  //determines how much to add to the pot
  //and how much to remove from the player's
  //wallet
  private void BettingAlgorithm(Casino c)
  {
    if(valueBetted == 0)
    {
      valueBetted = c.betMin + raised;
      wallet.Withdrawl(valueBetted);
      c.pot.Deposit(valueBetted);
    }
    else
    {
      wallet.Withdrawl(c.betMin + raised - valueBetted);
      c.pot.Deposit(c.betMin + raised - valueBetted);
      valueBetted = c.betMin + raised;
    } 
    
    if(valueBetted > c.betMin)
    {
      c.betMin = valueBetted;
    }
    
    for(int i = 0; i < c.players.length; i++)
      c.players[i].WalletFuture(c);

    c.potMenu.setText("Pot is currently: $" + c.pot.GetValue());
    displayWallet.setText("Wallet: $" + wallet.GetValue());
    c.betMenu.setText("Required Bet: $" + c.betMin);
  }

  //sets visibility to on or off
  //depending on what the percieved
  //round is
  public void BettingRound()
  {
    waitOnOthers.setVisible(false);
    commitDraw.setVisible(false);
    betBox.setVisible(true);
    betAmount.setValue(0);
    
    for(int i = 0; i < hand.GetHandSize(); i++)
      throwAway[i].setVisible(false);
  }

  //same idea as betting round, different
  //setup
  public void DrawRound()
  {
    waitOnOthers.setVisible(false);
    betBox.setVisible(false);
    commitDraw.setVisible(true);

    for(int i = 0; i < hand.GetHandSize(); i++)
      throwAway[i].setVisible(true);
  }

  //turns everything off, says the player is waiting
  public void Waiting()
  {
    betBox.setVisible(false);
    commitDraw.setVisible(false);

    for(int i = 0; i < hand.GetHandSize(); i++)
      throwAway[i].setVisible(false);

    waitOnOthers.setText("Waiting On Other Players");
    waitOnOthers.setVisible(true);
  }

  //turns everything off, says the player folded
  public void PlayerFolded()
  {
    Waiting();
    waitOnOthers.setText("Player Folded");
  }

  public int GetValueBetted()
  {
    return valueBetted;
  }

  //prints out the cards and makes checkboxes
  //to see if the cards should be thrown away
  public void PrintCards()
  {
    for(int i = 0; i < hand.GetHandSize(); i++)
      cardPictures[i].setIcon(hand.GetImage(i));
  }

  public void HideCards()
  {
    for(int i = 0; i < hand.GetHandSize(); i++)
      cardPictures[i].setIcon(hand.GetHiddenImage(i));
  }

  public void PrintHand()
  {
    for(int i = 0; i < hand.GetHandSize(); i++) 
      System.out.println(hand.PrintCard(i));
  }

  public int GetHandRanking()
  {
    return hand.GetHandRanking();
  }

  public String GetName()
  {
    return name;
  }

  public void WalletFuture(Casino casino)
  {
    if(valueBetted == 0)
    {
      displayDifference.setText("Wallet after you bet: $" + 
        (wallet.GetValue() - casino.betMin - raised));
    }
    else
    {
      displayDifference.setText("Wallet after you bet: $" +
        (wallet.GetValue() - valueBetted + casino.betMin - raised));
    }
  }

  private void CheckForNextRound(Casino casino)
  {
    casino.waitCounter = 0;
    casino.showdownCounter = 0;

    for(int j = 0; j < casino.players.length; j++)
    {
      if(casino.players[j].valueBetted < casino.betMin && 
             casino.waitCounter != casino.players.length && 
             casino.players[j].folded == false)
      {
        casino.players[j].BettingRound();
      }
    }

    for(int i = 0; i < casino.players.length; i++)
    {
      //counts the number of people waiting for the other
      //players to finish
      if(casino.players[i].waitOnOthers.isVisible() ||
             casino.players[i].folded == true)
      {
        casino.waitCounter++;
      }

      //counts the number of people ready to showdown
      if(casino.players[i].showdown == true ||
             casino.players[i].folded == true)
      {
        casino.showdownCounter++;
      }
    } 
    //takes everybody to the drawing round of the game if
    //everybody is waiting, and nobody is ready to showdown
    if(casino.showdownCounter < casino.players.length && 
           casino.waitCounter == casino.players.length)
    { 
      for(int m = 0; m < casino.players.length; m++)
      {
        if(casino.players[m].folded == false)
          casino.players[m].DrawRound();
      }
    }
    //runs if it is time for the showdown
    else if(casino.showdownCounter == casino.players.length &&
               casino.waitCounter == casino.players.length)
    {
      //gets winner and transfers the pot
      casino.SortWinners();

            
      for(int r = 0; r < casino.players.length && casino.pot.GetValue() > 0; r++)
      {
        //gives the winner the money if they haven't folded
        if(casino.players[r].folded == false)
        {
            //shows the player who won 
            casino.replayOption.showConfirmDialog(casino.replayFrame, 
            casino.players[r].name + " won " + 
            casino.pot.GetValue(),"Winner", casino.replayOption.OK_CANCEL_OPTION);
            casino.players[r].wallet.Transfer(casino.pot);
            casino.players[r].displayWallet.setText("Wallet: $" + 
                casino.players[r].wallet.GetValue());
            break;
        }
      } 
     
      //runs if the players want to play again
      if(casino.replayOption.showConfirmDialog(casino.replayFrame, 
          "\nWould you like to play another hand?","Play Again?", 
          casino.replayOption.YES_NO_OPTION) == casino.replayOption.YES_OPTION)
      {
        casino.betMin = casino.ANTE;
        casino.potMenu.setText("Pot is currently: $" + casino.pot.GetValue());
        casino.betMenu.setText("Required Bet: $" + casino.betMin);
        casino.cardDealer.ShuffleDeck();
              
        for(int p = 0; p < casino.players.length; p++)
        {
          //dealing the players new cards
          for(int q = 0; q < casino.players[p].hand.GetHandSize(); q++)
            casino.players[p].hand.SetCard(casino.cardDealer.DealCard(), q); 
               
          //updates graphics/setup for next game
          casino.players[p].showdown = false;
          casino.players[p].valueBetted = 0;
          casino.players[p].timesRepeated = 0;
          casino.players[p].raised = 0;
          casino.players[p].folded = false;
          casino.players[p].bestHand.setText(casino.players[p].hand.FindHandRanking());
          casino.players[p].PrintCards();
          casino.players[p].betAmount.setMinimum(0);
          casino.players[p].betAmount.setValue(0);
          casino.players[p].betAmount.setMaximum (casino.players[p].wallet.GetValue());
          casino.players[p].BettingRound();
        }
          casino.SortWinners();
      }
      else
        System.exit(1);
    }
  }

  //name of player
  public String name;

  //player's hand of cards
  private CardHand hand;

  public int timesRepeated;

  private JLabel[] cardPictures;
  private JLabel bestHand;
  private JCheckBox[] throwAway;

  //Required to show player's wallet
  public BankAccount wallet;
  private Box walletBox;
  public JLabel displayWallet;
  public JLabel displayDifference;

  public boolean showdown;
  
  //Required to show the betting amount
  public JSlider betAmount;
  private Box betBox;
  public JButton commitBet;
  public int valueBetted;
  public int raised;
  public int localBetMin;
  public JButton foldButton;
  public boolean folded;

  //Required to allow players to throw away cards and get new ones
  private JButton commitDraw;

  //Tells user to wait on other players
  public JLabel waitOnOthers;

  private BorderLayout superLayout;

  private JPanel bottomOfPage; 
  private Box[] cardBoxes;
  
  private JPanel centerOfPage;
  private GridBagConstraints constraints;
  private JButton flipCards;

  private Color appColor;
}
