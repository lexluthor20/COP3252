import java.util.ArrayList;
import java.util.Scanner;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileNotFoundException;
import java.io.EOFException;
import java.io.IOException;

public class StudentHandeler
{
  public StudentHandeler()
  {
    //creates a new arrayList
    students = new ArrayList<Student>();
  }

  public void saveStudents(Scanner s)
  {
    try
    {
      String filename = new String();
      System.out.print("Please input the filename to save to: ");
      filename = s.next();
    
      //seems like boilerplate code you have to write to write
      //objects to a file
      FileOutputStream fos = new FileOutputStream(filename);
      ObjectOutputStream oos = new ObjectOutputStream(fos);

      //writing all of the objects to the file
      for(int i = 0; i < students.size(); i++)
        oos.writeObject(students.get(i));

      //closing the file
      oos.close();
    }
    catch(FileNotFoundException e)
    {
      System.out.println("Error: File not found");
      s.nextLine();
    }
    catch(IOException e)
    {
      System.out.println("Error writing file");
      s.nextLine();
    }

  }

  void loadStudents(Scanner s)
  {

    try
    {
      String filename = new String();
      System.out.print("Please input the filename to load from: ");
      filename = s.next();
    
      FileInputStream fis = new FileInputStream(filename);
      ois = new ObjectInputStream(fis);

      //creates new object until an IOException is caught
      while(true)
      {
        students.add((Student) ois.readObject());
        Student.incrementStudentCount();
      }
    }
    catch(FileNotFoundException e)
    {
      System.out.println("Error: File not found");
      s.nextLine();
    }
    catch(EOFException e)
    {
      //catching the EOFException from the while loop
    }
    catch(IOException e)
    {
      System.out.println("Error: IOExcepton");
      e.printStackTrace();
    }
    catch(ClassNotFoundException e)
    {
      System.out.println("Error: Unable to find class");
      s.nextLine();
    }
    finally
    {
      //ends by closing the file
      try
      {
        ois.close();

        for(int i = 0; i < students.size(); i++)
          students.get(i).calcGrade();
      }
      catch (NullPointerException e)
      {
        //catches the exception caused from trying to close a file
        //that doesn't exist
      }
      catch(IOException e)
      {
        System.out.println("Error: Unable to close file");
        s.nextLine();
      }
    }
  }

  public void addStudent(Scanner s)
  {
    String first = new String();
    String last = new String();
    double num;
    Student stu;

    while(true)
    {
      System.out.print("Please input a first name: ");
      first = s.next();
      System.out.println();
      System.out.print("Please input a last name: ");
      last = s.next();
      System.out.println();
   
      stu = new Student(first,last);
    
      //getting the student's homeworks
      System.out.print("Please input student homework grades one at a time");
      System.out.print(" (negative value to finish) : ");
    
      try
      {
        num = s.nextDouble();
            
        if(num >= 0)
          stu.addHW(num);

        System.out.println();

        while(num >= 0)
        {
          System.out.print("Please add another homework grade");
          System.out.print(" (negative value to finish) : ");
          num = s.nextDouble();
          System.out.println();

          if(num < 0)
            break;
          else
            stu.addHW(num);
        }

        //getting the student's tests
        System.out.print("Please input student test grades one at a time");
        System.out.print(" (negative value to finish) : ");
        num = s.nextDouble();
        System.out.println();

        if(num >= 0)
          stu.addTest(num);

        while(num >= 0)
        {
          System.out.print("Please add another test grade");
          System.out.print(" (negative value to finish) : ");
          num = s.nextDouble();
          System.out.println();

          if(num < 0)
            break;
          else
            stu.addTest(num);
        }

        //adding new student to student ArrayList
        students.add(stu);

        //breaks the infinite loop
        break;
      }
      catch(InputMismatchException e)
      {
        System.out.println("Invalid input, please try inputting the student again");
        //Student must be decremented if
        //an error occurs, since it is
        //auto incremented in the constructor
        Student.decrementStudentCount();
        s.nextLine();
      }
    }

  }

  void printAllStudents()
  {
    //anonymous class that sorts the arrayList
    students.sort(new Comparator<Student>()
        {
          public int compare(Student s1, Student s2)
          {
            if(s1.getLast().compareTo(s2.getLast()) == 0)
              return s1.getFirst().compareTo(s2.getFirst());
            else
              return s1.getLast().compareTo(s2.getLast());
          }
        });

    System.out.println();

    for(int i = 0; i < students.size(); i++)
      System.out.println(students.get(i));

    System.out.println("\nPrinted " + Student.getNumStudents() + " Student Records\n");
  }

  void clearAllStudents()
  {
    //removes all elements from the list
    students.clear();
    //sets student count to zero
    Student.resetStudentCount();
  }

  public static void main(String[] args)
  {
    StudentHandeler sh = new StudentHandeler();
    Scanner input = new Scanner(System.in);
    int num;

    do
    {
      Menu();
      try
      {
        num = input.nextInt();
      }
      catch(InputMismatchException e)
      {
        System.out.println("Invalid choice, try again");
        input.nextLine();
        num = -1;
      }

      switch(num)
      {
        case(1):
          sh.printAllStudents();
          break;
        case(2):
          sh.addStudent(input);
          break;
        case(3):
          sh.clearAllStudents();
          break;
        case(4):
          sh.saveStudents(input);
          break;
        case(5):
          sh.loadStudents(input);
          break;
        case(6):
          System.out.println("Goodbye!");
          System.exit(1);
      }

    }while(num != 6);
  }

  //Prints the menu
  private static void Menu()
  {
    System.out.println("\n1: Print out all loaded students");
    System.out.println("2: Add student");
    System.out.println("3: Clear students");
    System.out.println("4: Save students to file");
    System.out.println("5: Load students from file");
    System.out.println("6: Quit\n");
    System.out.print("Please input the number of your choice: ");
  }

  private ArrayList<Student> students;
  private ObjectInputStream ois;
}
