//Alex Dupper

class IntegerSet
{
  //constructor, setting the size of the
  //array, and setting all values to false
  IntegerSet()
  {
    array = new boolean[MAX_SIZE];

    for(int i = 0; i < MAX_SIZE; i++)
      array[i] = false;
  }

  //union function. creating a temp object
  //to store any true values in either 
  //array and returning the object
  public IntegerSet union(IntegerSet iSet)
  {
    IntegerSet temp = new IntegerSet();

    //for loop setting temp's values to true
    //if a true is found in either set
    for(int i = 0; i < MAX_SIZE; i++)
      if(array[i] == true || iSet.array[i] == true)  
        temp.array[i] = true;

    return temp;
  }

  //intersection function. Creating a temp object
  //to store true values if both sets contain the
  //value, and returning the object
  public IntegerSet intersection(IntegerSet iSet)
  {
    IntegerSet temp = new IntegerSet();

    //for loop setting temp's values to true
    //if both sets contain true for said value
    for(int i = 0; i < MAX_SIZE; i++)
      if(array[i] == true && iSet.array[i] == true)
        temp.array[i] = true;

    return temp;
  }
  
  //insertElement function. sets value to true
  //at the element number of the value passed in
  public IntegerSet insertElement(int data)
  {
    array[data] = true;
    return this;
  }

  //deleteElement function. sets value to false
  //at the element number of the value passed in
  public IntegerSet deleteElement(int data)
  {
    array[data] = false;
    return this;
  }

  //isEqualTo function. returns false the 
  //first time a mismatch between the arrays
  //is seen. returns true if the whole loop
  //is run through
  public boolean isEqualTo(IntegerSet iSet)
  {
    for(int i = 0; i < MAX_SIZE; i++)
      if(array[i] != iSet.array[i])
        return false;

    return true;
  }

  public String toString()
  {
    String temp = new String();

    for(int i = 0; i < MAX_SIZE; i++)
      if (array[i] == true)
        temp = temp+(' ' + String.valueOf(i));
   
    if(temp.isEmpty() == false)
      return temp;

    else
    {
      temp = "---";
      return temp;
    }
  }

  private boolean[] array;
  private static final int MAX_SIZE = 101;
};
