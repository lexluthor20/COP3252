
// Author:  Bob Myers
 
// For COP3252, Java Programming

public class Fraction
{
  private int numerator = 0;		// numerator (and keeps sign)
  private int denominator = 1;		// always stores positive value

  public Fraction()
  {
  }

  public Fraction(int n, int d)
  {
    if (set(n,d)==false)
      set(0,1);
  }

  public boolean set(int n, int d)
  {
    if (d > 0)
    {
      numerator = n;
      denominator = d;
      return true;
    }
    else
      return false;
  }

  public String toString()
  {
    return (numerator + "/" + denominator);
  }

  public int getNumerator()
  {
    return numerator;
  }

  public int getDenominator()                   	              			    
  {
    return denominator;
  }

  public double decimal()
  {
    return (double)numerator / denominator;
  }

  //everything below here is my own

  //finding the gcd with base case of denominator,
  //and shrinking the value unitl there is no
  //smaller value that divides evenly into both
  //numerator and denominator
  public Fraction simplify()
  {
    Fraction temp = new Fraction();
    int den = denominator;

    temp.numerator = numerator;
    temp.denominator = denominator;

    while((numerator%den != 0) || (denominator%den != 0))
      den--;
    
    temp.numerator = temp.numerator/den;
    temp.denominator = temp.denominator/den;

    return temp;
  }
  
  //creates a temporary fraction, then cross multiplies,
  //adds both numerators together, gets a common denominator
  //and finally simplifies
  public Fraction add(Fraction f)
  {
    Fraction temp = new Fraction();

    temp.numerator = (numerator*f.denominator) + (denominator*f.numerator);
    temp.denominator = denominator * f.denominator;

    negsign(temp);

    return temp.simplify();
  }

  //same as the add function, but it subtracts instead
  //of adding
  public Fraction subtract(Fraction f)
  {
    Fraction temp = new Fraction();

    temp.numerator = (numerator*f.denominator) - (denominator*f.numerator);
    temp.denominator = denominator * f.denominator;

    negsign(temp);

    return temp.simplify();
  }

  //multiplies calling fraction by fraction
  //passed in, and returns a temp fraction
  public Fraction multiply(Fraction f)
  {
    Fraction temp = new Fraction();

    temp.numerator= numerator * f.numerator;
    temp.denominator = denominator * f.denominator;

    negsign(temp);

    return temp.simplify();
  }

  //dividing fractions is same as
  //cross multipling, so cross multiply
  //is implemented, returning calling
  //fraction
  public Fraction divide(Fraction f)
  {
    Fraction temp = new Fraction();

    //sets value to 0/1 if either numerator is 0
    if(numerator == 0 || f.numerator == 0)
    {
      temp.numerator = 0;
      temp.denominator = 1;
    }

    else
    {
      temp.numerator = numerator * f.denominator;
      temp.denominator = denominator * f.numerator;
    }

    negsign(temp);

    return temp.simplify();
  }

  //helper function that switches where
  //the negative sign is
  private void negsign(Fraction f)
  {
    if(f.denominator < 0)
    {
      f.denominator = f.denominator*-1;
      f.numerator = f.numerator*-1;
    }
  }
}
